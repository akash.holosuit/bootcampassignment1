﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuilderScript : MonoBehaviour
{
    public Sprite[] menuSprites;
    public string[] menuTitle;
    public GameObject[] menuObjectPrefab;
    public Dictionary<string, GameObject> dictionary;

    public GameObject objectInScene;
    public Transform[] spawnPoint;
    public GameObject menuPrefab;
    public GameObject menuButtonPanel;
    public GameObject menuItemPanel;
    public GameObject textElement;
    private GameObject UIList;

    // Start is called before the first frame update
    void Start()
    {
        BuildMenu();
    }

  


    void BuildMenu()
    {
        if (menuSprites.Length == menuTitle.Length)
        {

            for (int i = 0; i < menuSprites.Length; i++)
            {
                GameObject gO = Instantiate(menuPrefab, menuButtonPanel.transform);

                gO.GetComponent<Image>().sprite = menuSprites[i];
                gO.GetComponentInChildren<Text>().text = menuTitle[i];
                gO.name = menuTitle[i];
            }
        }
        CreateDictionary();
    }

    void CreateDictionary()
    {
        if (menuObjectPrefab.Length == menuTitle.Length)
        {
            dictionary = new Dictionary<string, GameObject>();

            for (int i = 0; i < menuTitle.Length; i++)
            {
                dictionary.Add(menuTitle[i], menuObjectPrefab[i]);
                
            }
            
        }
    }


    public void SpawnObject(string key)
    {
        
        int index = Random.Range(0, 4);
        Debug.Log("entered the funtion with index =" + index);

        objectInScene = Instantiate(dictionary[key], spawnPoint[index].position , dictionary[key].transform.rotation);
        UIList = Instantiate(textElement, menuItemPanel.transform);
        UIList.GetComponent<Text>().text = key;
        
    }

    public void HighlightObject()
    {

    }

    
}
