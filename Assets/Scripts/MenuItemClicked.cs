﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuItemClicked : MonoBehaviour
{
  public void CallSpawnObject()
    {
        GameObject go = GameObject.Find("MenuBuilder");
        go.GetComponent<BuilderScript>().SendMessage("SpawnObject", gameObject.name);
        Debug.Log("the name of the gameobject" + gameObject.name);
       
    }
}
