﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnListItemClicked : MonoBehaviour
{

    public void CallHighlightObject()
    {
        BuilderScript go = GameObject.Find("MenuBuilder").GetComponent<BuilderScript>();
        go.HighlightObject();

    }   
}
