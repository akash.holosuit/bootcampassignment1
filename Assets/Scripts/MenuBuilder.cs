﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuBuilder : MonoBehaviour
{

    public Sprite[] menuSprites;

    public string[] menuTitleTexts;

    public GameObject[] menuPrefabs;

    public Dictionary<string, GameObject> spawnDictionary;

    public GameObject objectInScene;

    public Transform spawnPoint;

    public GameObject menuItemPrefab;

    public GameObject menuPanel;


    // Start is called before the first frame update
    void Start()
    {
        BuildMenu();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void BuildMenu()
    {
        if(menuSprites.Length == menuTitleTexts.Length)
        {
            int length = menuSprites.Length;

            for (int i=0; i<length; i++)
            {
                GameObject gO = Instantiate(menuItemPrefab, menuPanel.transform);
                gO.GetComponent<Image>().sprite = menuSprites[i];
                gO.GetComponentInChildren<Text>().text = menuTitleTexts[i];
                gO.name = menuTitleTexts[i];
            }
        }

        CreateSpawnDictionary();

    }

    void CreateSpawnDictionary()
    {
        if(menuPrefabs.Length == menuTitleTexts.Length)
        {
            spawnDictionary = new Dictionary<string, GameObject>();

            for(int i=0; i<menuPrefabs.Length; i++)
            {
                spawnDictionary.Add(menuTitleTexts[i], menuPrefabs[i]);
            }
            Debug.Log("spawn Dictionary created");

           





        }
    }


    public void SpawnObject(string key)
    {
        if (objectInScene != null)
            Destroy(objectInScene);
        objectInScene = Instantiate(spawnDictionary[key], spawnPoint.position, spawnDictionary[key].transform.rotation);
    }
    
}

